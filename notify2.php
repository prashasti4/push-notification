<?PHP
	function sendMessage(){
		$content = array(
			"en" => 'Test notifications for closed/minimized browsers '
			);
		
		$fields = array(
			'app_id' => "6582674e-afea-44f4-8baa-c24b909bbe6c",
			'included_segments' => array('All'),
      'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
    print("\nJSON sent:\n");
    print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic YTMwOGJhYzgtMjA0OC00ZjNhLTkwN2YtNjU1NjgxODhmYmJh'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
	$response = sendMessage();
	$return["allresponses"] = $response;
	$return = json_encode( $return);
	
  print("\n\nJSON received:\n");
	print($return);
  print("\n");
?>

<html>
<head>
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script>
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "6582674e-afea-44f4-8baa-c24b909bbe6c",
      autoRegister: true, /* Set to true to automatically prompt visitors */
      subdomainName: 'chat2',   
      notifyButton: {
          enable: false /* Set to false to hide */
      }
    }
    ]);
 
  </script>
</head>
<body>
  Web Push Notifications 
</body>
</html>